# A Simple Notifications With Python

<div align="center">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/200px-Python-logo-notext.svg.png" weight=200px />
</div>

This is a simple program that generates Twitter notifications made in python

## Used Modules

- [notifypy](https://pypi.org/project/notify-py/) (Used to create the notification)