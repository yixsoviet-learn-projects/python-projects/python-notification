from notifypy import Notify

# Variable
twitterPerson = str(input('Insert username of person: '))


notification = Notify()  # Variable to use Notify

notification.title = "Twitter"  # Generates the title of the notification.

# Generates the notification message.
notification.message = f"@{twitterPerson} is now following you!"

notification.icon = "./img/twitter_logo.png"  # Notification icon

notification.send()  # Send the notification.
